7-Minute Workout application for Garmin watches.

This watch application drives you through the execution of 13 exercises described in literature as High-Intensity Circuit Training.

The application displays a timer for the 30-second exercises followed by 10-second rest period (configurable).

----------
LAST-MINUTE NOTES based on user reviews:
* vivoactive HR users have reported issues with HRM detection. This concerns all ConnectIQ apps: https://forums.garmin.com/showthread.php?350783-vivoactive-hr-not-starting-the-optical-sensor-for-CIQ-apps
* Work/rest intervals are configurable!
* Calories will not be calculated without heart-rate monitor (HRM)!
----------

Duration of exercise and rest periods is configurable through Garmin Connect Mobile.
A long vibration will trigger at the beginning and at the end of each exercise.
A short vibration will trigger every 10 seconds during the exercise.
Sound notification is not supported.

The application supports heart-rate monitor (HRM) and temperature sensor (Tempe).

If you achieve at least 6 exercises, the activity is recorded as a training activity in your Garmin profile.

The activity in your Garmin profile will include basic information such as date, time, total activity time and laps.
    - Each exercise and rest period corresponds to a lap.
    - Heart-rate graph is included  if a heart-rate monitor is used.
    - Temperature graph is included if a temperature sensor is used.
    - Calories will be automatically calculated if and only if a heart-rate monitor is used.

Use the configuration in Garmin Connect or Garmin Express to customize the work/rest intervals.

== SUPPORT ==

Please submit questions, issues and enhancement requests on the Bitbucket project : https://bitbucket.org/obagot/connectiq-hict/issues
Supported languages: English, French.
Supported models: vívoactive®, vívoactive® HR, Forerunner® 920XT, 630, 235, 230, epix™, fēnix® 3, fēnix® 3 HR.
License : https://opensource.org/licenses/MIT


== DEVELOPMENT ==

Source code is made available under the MIT license on the Bitbucket project : https://bitbucket.org/obagot/connectiq-hict
Pull requests and translations are welcomed!
