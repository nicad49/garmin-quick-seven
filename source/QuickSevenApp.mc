using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class QuickSevenApp extends App.AppBase {

    function initialize() {
        AppBase.initialize();
        
        // Update app version in preferences
        var version = Ui.loadResource(Rez.Strings.AppVersion);
        setProperty("appVersion", version);
    }

    // onStart() is called on application start up
    function onStart(state) {
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    }

    // Return the initial view of your application here
    function getInitialView() {
        view = new QuickSevenView();
        behaviorDelegate = new QuickSevenDelegate();
        view.loadPreferences();
        behaviorDelegate.setQuickSevenView(view);
        
        return [view, behaviorDelegate ];
    }
    
    //! New app settings have been received
    function onSettingsChanged() {
    	view.loadPreferences();
    	Ui.requestUpdate();
    }
    
    hidden var view;
    hidden var behaviorDelegate;

}
